docker pull asmody/mista-adv-mgmt:latest
docker stop mista-adv-mgmt
docker rm mista-adv-mgmt
docker rmi asmody/mista-adv-mgmt:current
docker tag asmody/mista-adv-mgmt:latest asmody/mista-adv-mgmt:current
docker run -d --name mista-adv-mgmt --env-file=/var/www/.env -p 3000:3000 -v /var/run/mysqld:/var/run/mysqld -v /var/www/mista.forum/advices:/app/data asmody/mista-adv-mgmt:current