// const User = require('./User')

const accessTable = [
  {
    userId: 850,
    read: () => true,
    create: () => true,
    update: () => true,
    delete: () => true
  }
]

const defaultRigths = {
  read: () => true,
  create: () => false,
  update: () => false,
  delete: () => false
}

class Access {
  constructor (userId) {
    this.userId = userId
    const rights = accessTable.find((i) => (i.userId === this.userId))
    this.rights = rights || defaultRigths
  }

  static check (right, userId, row) {
    const access = new Access(userId)
    return access.rights[right] && access.rights[right](row)
  }
}

module.exports = Access
