const md5 = require('blueimp-md5')
const { mysql } = require('../db/mistadb')
const { mista: { secretKey } } = require('../config')

function encryptedPass (password) {
  return md5(password + secretKey)
}

class User {
  constructor ({id, name}) {
    this.id = id
    this.name = name
  }

  static findOne (username) {
    return mysql.query('SELECT id, name FROM forum_users WHERE name = ?', [username])
      .spread((rows) => {
        if (rows.length > 0) {
          return new User(rows[0])
        } else {
          return null
        }
      })
  }

  comparePassword (password) {
    const encPass = encryptedPass(password)
    return mysql.query('SELECT id FROM forum_users WHERE id = ? AND pass_md5 = ?', [this.id, encPass])
      .spread((rows) => {
        return rows.length > 0
      })
  }

  toJSON () {
    let values = {...this}
    return values
  }
}

module.exports = User
