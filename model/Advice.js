const yaml = require('js-yaml')
const fs = require('fs')
const path = require('path')
const uuid = require('uuid/v4')

const { advicesPayYml } = require('../config')

const readAdvices = () => {
  const advicesYml = path.normalize(path.join(__dirname, '..', advicesPayYml))
  const advices = yaml.safeLoad(fs.readFileSync(advicesYml, 'utf8'))
  return advices.map((itm) => new Advice(itm))
}

const writeAdvices = (advices) => {
  const advicesYml = path.normalize(path.join(__dirname, '..', advicesPayYml))
  fs.writeFileSync(advicesYml, yaml.safeDump(advices), 'utf8')
}

class Advice {
  /* eslint-disable camelcase */
  constructor ({id, date_to, text, place}) {
    this.id = id
    this.date_to = date_to
    this.text = text
    this.place = place
  }
  /* eslint-enable */

  save () {
    let advices = readAdvices()
    if (!this.id) {
      this.id = uuid()
      advices.push(Object.assign({}, this))
    } else {
      advices = advices.map((itm) => itm.id === this.id ? Object.assign({}, this) : itm)
    }
    writeAdvices(advices)
  }

  drop () {
    if (this.id) {
      let advices = readAdvices()
      advices = advices.filter((itm) => itm.id !== this.id)
      writeAdvices(advices)
    } else {
      throw new Error('ID not assigned on delete')
    }
  }

  static find (id) {
    return readAdvices().find((itm) => itm.id === id)
  }

  static getAll () {
    return readAdvices()
  }
}

module.exports = Advice
