const advicesRouter = require('./advices')
const authRouter = require('./auth')

module.exports = (app) => {
  app.use('/api/advices', advicesRouter)
  app.use('/api/auth', authRouter)
}
