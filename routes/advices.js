const express = require('express')
const router = express.Router()

const AdvicesController = require('../controllers/AdvicesController')
const AuthenticationController = require('../controllers/AuthenticationController')
const AccessController = require('../controllers/AccessController')

router.get('/', AdvicesController.index)
router.post('/',
  [
    AuthenticationController.verifyToken,
    AccessController.allowCreate
  ],
  AdvicesController.create)

router.get('/:id',
  [
    AdvicesController.lookup,
    AccessController.allowRead
  ],
  AdvicesController.one)

router.put('/:id',
  [
    AuthenticationController.verifyToken,
    AdvicesController.lookup,
    AccessController.allowUpdate
  ],
  AdvicesController.update)

router.delete('/:id',
  [
    AuthenticationController.verifyToken,
    AdvicesController.lookup,
    AccessController.allowDelete
  ],
  AdvicesController.delete)

module.exports = router
