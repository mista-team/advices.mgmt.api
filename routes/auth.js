const express = require('express')
const router = express.Router()

const AuthenticationController = require('../controllers/AuthenticationController')

router.post('/signin', AuthenticationController.signin)
router.post('/verify',
  AuthenticationController.verifyToken,
  AuthenticationController.verify
)

module.exports = router
