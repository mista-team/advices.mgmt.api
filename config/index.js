require('dotenv').config()

module.exports = {
  advicesPayYml: './data/advices_pay.yml',
  mista: {
    db: {
      socketPath: process.env.DB_SOCKET,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_BASE
    },
    'secretKey': process.env.SECRET_KEY
  },
  port: 3000
}
