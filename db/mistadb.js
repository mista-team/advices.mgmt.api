const mysql2 = require('mysql2')
const mysqlPromise = require('mysql-promise')()
const { mista } = require('../config')

mysqlPromise.configure(mista.db, mysql2)

const checkMysqlConnection = () => {
  return mysqlPromise.query('SELECT 1')
}

const init = () => {
  return Promise.all([
    checkMysqlConnection()
  ])
}

module.exports = {
  mysql: mysqlPromise,
  init: init
}
