const Access = require('../model/Access')
const Advice = require('../model/Advice')

module.exports = {
  allowRead (req, res, next) {
    const {id} = req.decoded
    const advice = req.advice
    if (!Access.check('read', id, advice)) {
      return res.status(403).send({
        error: 'Access denied'
      })
    }
    next()
  },
  allowCreate (req, res, next) {
    const {id} = req.decoded
    const advice = new Advice(req.body)
    if (!Access.check('create', id, advice)) {
      return res.status(403).send({
        error: 'Access denied'
      })
    }
    next()
  },
  allowUpdate (req, res, next) {
    const {id} = req.decoded
    const advice = new Advice(req.body)
    if (!Access.check('update', id, advice)) {
      return res.status(403).send({
        error: 'Access denied'
      })
    }
    next()
  },
  allowDelete (req, res, next) {
    const {id} = req.decoded
    const advice = req.advice
    if (!Access.check('delete', id, advice)) {
      return res.status(403).send({
        error: 'Access denied'
      })
    }
    next()
  }
}
