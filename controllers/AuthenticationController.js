const jwt = require('jsonwebtoken')
const User = require('../model/User')
const { mista: { secretKey } } = require('../config')

function jwtSignUser (user) {
  const ONE_WEEK = 60 * 60 * 24 * 7
  return jwt.sign(user, secretKey, {
    expiresIn: ONE_WEEK
  })
}

function jwtVerifyToken (token, cb) {
  jwt.verify(token, secretKey, cb)
}

const allowedUsers = [1, 850]

module.exports = {
  async signin (req, res) {
    const { username, password } = req.body
    try {
      const user = await User.findOne(username)
      if (!user) {
        return res.status(403).send({
          error: 'The user not found'
        })
      } else if (!await user.comparePassword(password)) {
        return res.status(403).send({
          error: 'The password does not match'
        })
      } else if (!allowedUsers.includes(user.id)) {
        return res.status(403).send({
          error: 'Access denied'
        })
      } else {
        const userJson = user.toJSON()
        return res.json({
          user: userJson,
          token: jwtSignUser(userJson)
        })
      }
    } catch (error) {
      console.log(error)
      res.status(500).send({ error })
    }
  },
  verify (req, res) {
    const { id, name } = req.decoded
    return res.json({
      user: { id, name }
    })
  },
  verifyToken (req, res, next) {
    const token = req.body.token || req.query.token || req.headers['x-access-token']
    if (token) {
      jwtVerifyToken(token, (err, decoded) => {
        if (err) {
          return res.json({ success: false, message: 'Failed to authenticate token' })
        } else {
          req.decoded = decoded
          next()
        }
      })
    } else {
      return res.status(403).send({
        error: 'No token provided'
      })
    }
  }
}
