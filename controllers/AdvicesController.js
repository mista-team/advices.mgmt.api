const Advice = require('../model/Advice')

module.exports = {
  lookup (req, res, next) {
    const adviceId = req.params.id
    const advice = Advice.find(adviceId)
    if (advice === undefined) {
      res.statusCode = 404
      return res.json({ error: ['Advice not found'] })
    }
    req.advice = advice
    next()
  },
  index (req, res) {
    const advices = Advice.getAll()
    return res.json(advices)
  },
  one (req, res) {
    return res.json(req.advice)
  },
  create (req, res) {
    const advice = new Advice(req.body)
    try {
      advice.save()
      res.statusCode = 201
      return res.json(advice)
    } catch (e) {
      res.statusCode = 500
      return res.json({ error: 'Failed to write advices', message: e.message })
    }
  },
  update (req, res) {
    const advice = req.advice
    const adviceId = advice.id
    const newAdvice = new Advice(req.body)
    Object.assign(advice, newAdvice)
    advice.id = adviceId
    try {
      advice.save()
      return res.json(advice)
    } catch (e) {
      res.statusCode = 500
      return res.json({ error: 'Failed to write advices', message: e.message })
    }
  },
  delete (req, res) {
    const advice = req.advice
    try {
      advice.drop()
      return res.json({})
    } catch (e) {
      res.statusCode = 500
      return res.json({ error: 'Failed to delete advices', message: e.message })
    }
  }
}
