FROM node:alpine

WORKDIR /app

COPY package.json .
RUN npm i

COPY . .

RUN mkdir -p /var/run/mysqld
VOLUME /var/run/mysqld
VOLUME /app/data

EXPOSE 3000

CMD [ "npm", "start" ]
